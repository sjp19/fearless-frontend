function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col">
      <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class="text-muted">${startDate} - ${endDate}</small>
        </div>
      </div>
    </div>
    `;
  }

function itsAnError() {
    return `
    <div class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    
    try {
      const response = await fetch(url);
    //   console.log(response);
      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error("Response not ok");
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              console.log(details)
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;

              const startDate = new Date(details.conference.starts).toLocaleDateString();
              const endDate = new Date(details.conference.ends).toLocaleDateString();

              const location = details.conference.location.name;
              const html = createCard(title, description, pictureUrl, startDate, endDate, location);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
          }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    //   console.error("error", e)
      const errHtml = document.querySelector('.row');
      const err = itsAnError();
      errHtml.innerHTML += err;
    }
  
  });