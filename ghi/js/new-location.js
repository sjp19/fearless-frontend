window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/'
    const response = await fetch(url);
    if (response.ok){
        const data = await response.json();

        const stateSelect = document.getElementById('state');
        for (let state of data.states) {
            const stateOptions = document.createElement('option');
            stateOptions.value = state.abbreviation;
            stateOptions.innerHTML = state.name;
            stateSelect.appendChild(stateOptions);
        }


        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            //gets form data
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            
            //sends data to the server
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const locationResponse = await fetch(locationUrl, fetchConfig);
            if (locationResponse.ok) {
                formTag.reset();
                const newLocation = await locationResponse.json();
                console.log(newLocation);
            }
        });

    } else {
        throw new Error('State List Response Error')
    }
});