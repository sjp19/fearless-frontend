window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        const locationSelect = document.getElementById('location');
        for (let loc of data.locations) {
            const locationOptions = document.createElement('option');
            locationOptions.value = loc.id;
            locationOptions.innerHTML = loc.name
            locationSelect.appendChild(locationOptions);
        }

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();

            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const conferenceUrl = 'http://localhost:8000/api/conferences/'
            const fetchConfig = {
                method: "POST",
                body: json,
                headers: {'Content-Type': 'application/json',},
            };
            const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
            if (conferenceResponse.ok) {
                formTag.reset();
                const newConference = await conferenceResponse.json();
            }
        });

    } else {
        throw new Error('New Conferences not working')
    }
});